﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Turner_Developer_Challenge.Models;

namespace Turner_Developer_Challenge.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/

        public ActionResult Index()
        {
            using (TitlesEntities dc = new TitlesEntities()) 
            {
                var t = dc.Titles;
                List<Title> titles = t.ToList();
                return View(titles);
            }
        }

        public ActionResult Details(int id)
        {
            using (TitlesEntities dc=new TitlesEntities())
            {
                var t = from row in dc.Titles
                        where row.TitleId == id
                        select row;
                Title tit = t.First();
                return View(tit);
            }
        }

    }
}
